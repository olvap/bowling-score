# Description
This is a ruby 2 console app to calculate the socring in a bowling game.

# Install
You need ruby 2 installed in you machine.

Clone the repo

    git clone git@gitlab.com:olvap/bowling-score.git

Enter to de proyect folder

    cd bowling-score

Install dependencies

    gem install bundler

    bundle install

# Usage
You can set a file with data in a file call 'bowling-game.txt'

    ./bowling-score

You can also send a file as second parameter

    ./bowling-score PATH_TO_FILE
