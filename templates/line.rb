# frozen_string_literal: true

require './templates/frame'

module Templates
  class Line
    def initialize(line, separator: "\t", player:)
      @line = line
      @separator = separator
      @player = player
    end

    def to_s
      <<~OUTPUT
        #{header}
        #{player}
        #{pin_falls}
        #{score}
      OUTPUT
    end

    private

    attr_reader :line, :separator, :player

    def build_row(name, values)
      [name, values].join(separator)
    end

    def header
      name = "frame#{separator}"
      values = (1..10).to_a.map { |index| "#{index}#{separator}" }
      build_row(name, values)
    end

    def pin_falls
      build_row(
        'PinFalls',
        line.first(10).map { |frame| Templates::Frame.new(frame).to_s }
      )
    end

    def score
      build_row(
        "score#{separator}",
        line.first(10).map(
          &:accumulative_score
        ).map { |score| "#{score}#{separator}" }
      )
    end
  end
end
