# frozen_string_literal: true

module Templates
  class Frame
    attr_reader :frame

    def initialize(frame)
      @frame = frame
    end

    def to_s
      return "\tX" if frame.strike?
      return "#{frame.rolls.first}\t/" if frame.spare?

      frame.rolls.join("\t").gsub('10', 'X')
    end
  end
end
