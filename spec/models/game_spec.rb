# frozen_string_literal: true

require './models/game'
class TetsTemplate
  def initialize(_line, player:)
  end

  def to_s
  end
end

describe Game do
  let(:first_roll) { "jeff\t 5" }
  let(:second_roll) { "jeff\t 1" }
  let(:rolls) { [first_roll, second_roll] }
  let(:line) { Line.new }
  let(:template) { TetsTemplate }

  describe '#scores' do
    before do
      allow(Line).to receive(:new).and_return(line)
    end
    let(:game) { Game.new(file: rolls, template: template) }

    it 'call a template for the record' do
      expect(TetsTemplate).to receive(:new).with(
        line,
        player: :jeff
      )
      game.scores
    end
  end
end
