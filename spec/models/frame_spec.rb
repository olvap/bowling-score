# frozen_string_literal: true

require './models/frame'

describe Frame do

  describe '#strike?' do
    it 'to return true if the frame is a strike' do
      expect(Frame.new(10).strike?).to be_truthy
    end

    it 'to return false when the frame is no a strike' do
      expect(Frame.new(4, 6).strike?).to be_falsey
    end
  end

  describe '#spare?' do
    it 'to return true if the frame is a spare' do
      expect(Frame.new(4, 6)).to be_truthy
    end
  end

  it 'return and exception when pass more than 2 arguments' do
    expect { Frame.new(4, 1, 2) }.to raise_error(ArgumentError)
  end

  describe '#score' do
    it 'to sum both rolls to calculate the score' do
      expect(Frame.new(4, 5).score).to eq(9)
    end
    it 'to sum the rolls without next frame' do
      expect(Frame.new(4, 6).score).to eq(10)
    end

    context 'with a spare' do
      it 'take the next roll to calculate the score' do
        second_frame = Frame.new(4, 5)
        first_frame = Frame.new(4, 6, next_frame: second_frame)
        expect(first_frame.score).to eq(14)
      end
    end

    context 'with a strike' do
      it 'take the next roll to calculate the score' do
        second_frame = Frame.new(4, 5)
        first_frame = Frame.new(10, next_frame: second_frame)
        expect(first_frame.score).to eq(19)
      end

      it 'take the next roll to calculate the score' do
        third_frame = Frame.new(4, 1)
        second_frame = Frame.new(10, next_frame: third_frame)
        first_frame = Frame.new(10, next_frame: second_frame)
        expect(first_frame.score).to eq(24)
      end

      it 'the perfect score' do
        third_frame = Frame.new(10)
        second_frame = Frame.new(10, next_frame: third_frame)
        first_frame = Frame.new(10, next_frame: second_frame)
        expect(first_frame.score).to eq(30)
      end
    end
  end
end
