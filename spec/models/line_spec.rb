# frozen_string_literal: true

require './models/line'
require 'pry'

describe Line do
  let(:line) { Line.new }

  it 'to return the score from a single roll' do
    line.roll(6)
    expect(line.score).to be(6)
  end

  it 'to return the score from multiples rolls' do
    line.roll(6)
    line.roll(4)
    line.roll(4)
    expect(line.score).to be(18)
  end

  it 'perfect game' do
    12.times { line.roll(10) }
    expect(line.score).to be(300)
  end
end
