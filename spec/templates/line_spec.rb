# frozen_string_literal: true

require './templates/line'

describe Templates::Line do
  let(:line) { Line.new }
  let(:template) { Templates::Line.new(line, player: 'pablo') }
  it 'Set line template' do
    line.roll 10
    line.roll 4
    line.roll 3
    expect(template.to_s).to eq(
      <<~OUTPUT
        frame\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\t
        pablo
        PinFalls\t\tX\t4\t3\t
        score\t\t17\t\t24\t\t0\t
      OUTPUT
    )
  end
end
