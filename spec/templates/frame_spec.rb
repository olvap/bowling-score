# frozen_string_literal: true

require './templates/frame'

describe Templates::Frame do

  let(:frame) { Frame.new(4, 1) }

  it 'to print both rolls' do
    expect(Templates::Frame.new(frame).to_s).to eq("4\t1")
  end

  context 'when the frame is a strike' do
    let(:strike) { Frame.new(10) }
    it 'to print an X' do
      expect(Templates::Frame.new(strike).to_s).to eq("\tX")
    end
  end

  context 'when the frame is a spare' do
    let(:spare) { Frame.new(4, 6) }

    it 'to print an X' do
      expect(Templates::Frame.new(spare).to_s).to eq("4\t/")
    end
  end
end
