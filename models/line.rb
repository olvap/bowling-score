# frozen_string_literal: true

require './models/frame'

class Line
  include Enumerable

  def initialize
    @frame = Frame.new
    @first_frame = @frame
  end

  def roll(pins)
    return frame.push(pins) if last_frame?

    if frame.completed?
      new_frame = Frame.new(pins, prev_frame: frame)
      frame.next_frame = new_frame
      self.frame = new_frame
    else
      frame.push(pins)
    end
  end

  def each(&block)
    return to_enum(:each) unless block
    frame = first_frame
    while frame
      yield frame
      frame = frame.next_frame
    end
  end

  def score
    frame.accumulative_score
  end

  private

  attr_reader :first_frame
  attr_accessor :frame

  def last_frame?
    count > 10
  end
end
