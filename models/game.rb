# frozen_string_literal: true

require './models/line'

class Game
  def initialize(file:, template:)
    @file = file
    @template = template
    @players = {}
  end

  def scores
    populate_scores
    player_scores
  end

  private

  def player_scores
    players.map { |player, line| template.new(line, player: player).to_s }
  end

  def populate_scores
    file.each do |row|
      player, pins = row.split("\t")

      players[player.to_sym] ||= Line.new
      players[player.to_sym].roll(pins.to_i)
    end
  end

  attr_accessor :players, :file, :template
end

