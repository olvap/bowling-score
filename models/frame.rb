# frozen_string_literal: true

class Frame
  attr_reader :rolls
  attr_accessor :next_frame, :prev_frame

  def initialize(
    *rolls,
    next_frame: NULL_FRAME,
    prev_frame: NULL_FRAME
  )
    raise ArgumentError, 'to many rolls' if rolls.size > 2

    @next_frame = next_frame
    @prev_frame = prev_frame
    @rolls = rolls || []
  end

  def strike?
    rolls.count == 1 && rolls.first == 10
  end

  def spare?
    rolls.count == 2 && frame_score == 10
  end

  def pins(count)
    return 0 if rolls.empty?
    (rolls + next_frame.rolls).first(count).inject(&:+)
  end

  def score
    (rolls + [
      spare? ? next_frame.pins(1) : 0,
      strike? ? next_frame.pins(2) : 0
    ]).inject(&:+)
  end

  def accumulative_score
    return 0 if rolls.empty?
    prev_frame.accumulative_score + score
  end

  def push(pins)
    rolls << pins
  end

  def completed?
    frame_score >= 10 || rolls.size >= 2
  end

  private

  def frame_score
    rolls.inject(&:+) || 0
  end

  NULL_FRAME = new(next_frame: nil, prev_frame: nil)
end
